FROM eclipse-temurin:21-alpine

VOLUME /main-app
COPY ./target/back-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 3000
ENTRYPOINT ["java", "-jar","/app.jar"]