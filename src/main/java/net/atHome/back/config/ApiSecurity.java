package net.atHome.back.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import net.atHome.back.auth.UserDetailsServiceImpl;
import net.atHome.back.filter.JwtAuthFilter;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@OpenAPIDefinition(info = @Info(title = "Salsa Back Api", version = "v1"))
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, bearerFormat = "JWT", scheme = "bearer")
public class ApiSecurity {

  @Autowired
  private JwtAuthFilter authFilter;

  private CorsConfiguration defineCorsConf() {

    // TODO => restreindre les params CORS là c'est OPEN BAR :D

    var corsconf = new CorsConfiguration().applyPermitDefaultValues();
    List<String> allowedHeader = new ArrayList<String>();
    allowedHeader.add("GET");
    allowedHeader.add("POST");
    allowedHeader.add("PUT");
    allowedHeader.add("DELETE");

    corsconf.setAllowedMethods(allowedHeader);
    return corsconf;
  }

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
    return http
        .csrf(csrf -> csrf.disable())
        .cors(httpSecurityCorsConfigurer -> httpSecurityCorsConfigurer
            .configurationSource(request -> this.defineCorsConf()))
        .authorizeHttpRequests(requests -> requests.requestMatchers("/api/auth/**").permitAll())
        .authorizeHttpRequests(requests -> requests.requestMatchers("/swagger-ui/**", "/v3/api-docs/**").permitAll())
        .authorizeHttpRequests(requests -> requests.requestMatchers("/api/**").authenticated())
        .sessionManagement(management -> management.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
        .authenticationProvider(this.authenticationProvider())
        .addFilterBefore(authFilter, UsernamePasswordAuthenticationFilter.class)
        .build();
  }

  @Bean
  public AuthenticationProvider authenticationProvider() {
    DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
    authenticationProvider.setUserDetailsService(userDetailsService());
    authenticationProvider.setPasswordEncoder(passwordEncoder());
    return authenticationProvider;
  }

  @Bean
  public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
    return config.getAuthenticationManager();
  }

  @Bean
  public UserDetailsService userDetailsService() {
    return new UserDetailsServiceImpl();
  }

  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }
}
