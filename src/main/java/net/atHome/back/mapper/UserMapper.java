package net.atHome.back.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import net.atHome.back.dto.UserDto;
import net.atHome.back.model.User;

@Component
public class UserMapper {

  @Autowired
  private PasswordEncoder encoder;

  public User mapDtoUserToUser(UserDto userDto) {
    User user = new User();
    user.setEmail(userDto.getEmail());
    user.setPassword(encoder.encode(userDto.getPassword()));
    return user;
  }

}
