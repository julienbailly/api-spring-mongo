package net.atHome.back;

import net.atHome.back.model.Sauce;
import net.atHome.back.model.User;
import net.atHome.back.repository.SauceRepository;
import net.atHome.back.repository.UserRepository;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

@SpringBootApplication
public class SalsaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalsaApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(UserRepository repository, MongoTemplate mongoTemplate, SauceRepository sauceRepository) {
		return args -> {

			User user = new User("ju@gro.net", "1234");

			Query query = new Query();

			query.addCriteria(Criteria.where("email").is(user.getEmail()));

			List<User> userList = mongoTemplate.find(query, User.class);

			if (userList.isEmpty()) {
				repository.insert(user);
			}

			userList = repository.findAll();

			for (User u : userList) {
				System.out.println(u.getId());
				System.out.println("______________________________________________");
			}

			List<Sauce> sauceList = sauceRepository.findAll();

			for (Sauce s : sauceList) {
				System.out.println(s.getId());
				System.out.println("______________________________________________");
			}

		};
	}

}
