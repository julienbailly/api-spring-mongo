package net.atHome.back.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import net.atHome.back.model.Sauce;
import net.atHome.back.repository.SauceRepository;

@Service
public class SauceService {

  @Autowired
  SauceRepository sauceRepository;

  public List<Sauce> getAllSauces() {
    return sauceRepository.findAll();
  }

  public String saveSauce(Sauce sauceDto) {
    Sauce sauce = new Sauce();
    System.out.println(sauceDto);
    return "OK";
  }

}
