package net.atHome.back.model;

import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Sauce {
  @Id
  private String id;

  private String userid;

  private String name;

  private String manufacturer;
  private String description;
  private String mainPepper;
  private String imageUrl;
  private Integer heat;
  private Integer likes;
  private Integer dislikes;
  private Set<String> usersLiked;
  private Set<String> usersDisliked;

  public Sauce() {
  }

  public Sauce(String userid, String name, String manufacturer, String description, String mainPepper,
      String imageUrl, Integer heat, Set<String> usersLiked, Set<String> usersDisliked) {
    this.userid = userid;
    this.name = name;
    this.manufacturer = manufacturer;
    this.description = description;
    this.mainPepper = mainPepper;
    this.imageUrl = imageUrl;
    this.heat = heat;
    this.usersLiked = usersLiked;
    this.usersDisliked = usersDisliked;
    this.dislikes = usersDisliked.size();
    this.likes = usersLiked.size();
  }

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserid() {
    return this.userid;
  }

  public void setUserid(String userid) {
    this.userid = userid;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getManufacturer() {
    return this.manufacturer;
  }

  public void setManufacturer(String manufacturer) {
    this.manufacturer = manufacturer;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getMainPepper() {
    return this.mainPepper;
  }

  public void setMainPepper(String mainPepper) {
    this.mainPepper = mainPepper;
  }

  public String getImageUrl() {
    return this.imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public Integer getHeat() {
    return this.heat;
  }

  public void setHeat(Integer heat) {
    this.heat = heat;
  }

  public Integer getLikes() {
    return this.likes;
  }

  public void setLikes(Integer likes) {
    this.likes = likes;
  }

  public Integer getDislikes() {
    return this.dislikes;
  }

  public void setDislikes(Integer dislikes) {
    this.dislikes = dislikes;
  }

  public Set<String> getUsersLiked() {
    return this.usersLiked;
  }

  public void setUsersLiked(Set<String> usersLiked) {
    this.usersLiked = usersLiked;
  }

  public Set<String> getUsersDisliked() {
    return this.usersDisliked;
  }

  public void setUsersDisliked(Set<String> usersDisliked) {
    this.usersDisliked = usersDisliked;
  }

}
