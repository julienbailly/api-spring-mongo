package net.atHome.back.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import net.atHome.back.model.Sauce;

public interface SauceRepository extends MongoRepository<Sauce, String> {

}
