package net.atHome.back.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import net.atHome.back.auth.JwtService;
import net.atHome.back.auth.UserDetailsServiceImpl;
import net.atHome.back.dto.AuthResponse;
import net.atHome.back.dto.UserDto;
import net.atHome.back.mapper.UserMapper;
import net.atHome.back.model.User;
import net.atHome.back.repository.UserRepository;
import net.atHome.back.service.UserService;

@RestController
@Tag(name = "Gestion de l'utilisateur")
@RequestMapping("/api/auth")
public class UserController {

  @Autowired
  private UserDetailsServiceImpl service;

  @Autowired
  private UserService userService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserMapper userMapper;

  @Autowired
  private JwtService jwtService;

  @Autowired
  private AuthenticationManager authenticationManager;

  @PostMapping("/login")
  public AuthResponse authenticateAndGetToken(@RequestBody UserDto userDto) {
    Authentication authentication = authenticationManager
        .authenticate(new UsernamePasswordAuthenticationToken(userDto.getEmail(), userDto.getPassword()));
    if (authentication.isAuthenticated()) {
      AuthResponse authResponse = new AuthResponse();
      authResponse.setToken(jwtService.generateToken(userDto.getEmail()));
      authResponse.setUserId(userRepository.findByEmail(userDto.getEmail()).getId());
      return authResponse;
    } else {
      throw new UsernameNotFoundException("invalid user request !");
    }
  }

  @PostMapping("/signup")
  public void createAccount(@RequestBody UserDto userDto) {
    User user = userMapper.mapDtoUserToUser(userDto);
    userService.createUser(user);
  }

}
