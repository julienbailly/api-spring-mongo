package net.atHome.back.api;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.atHome.back.model.Sauce;
import net.atHome.back.service.SauceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@Tag(name = "Gestion des recettes de sauces")
@RequestMapping("/api")
public class SalsaController {

  @Autowired
  private SauceService sauceService;

  @GetMapping("/sauces")
  @Operation(security = @SecurityRequirement(name = "bearerAuth"))
  public List<Sauce> getAllProductions() {
    return sauceService.getAllSauces();
  }

  // @PostMapping(value = "/sauces", consumes = {
  // MediaType.MULTIPART_FORM_DATA_VALUE })
  // @Operation(security = @SecurityRequirement(name = "bearerAuth"))
  // public String storeSalsa(@RequestPart("sauce") Sauce sauce,
  // @RequestPart("image") MultipartFile image) {
  // return sauceService.saveSauce(sauce, image);
  // }

  @PostMapping(value = "/sauces", consumes = {
      MediaType.MULTIPART_FORM_DATA_VALUE })
  @Operation(security = @SecurityRequirement(name = "bearerAuth"))
  public String storeSalsa(@RequestPart("sauce") Sauce sauce) {
    // return sauceService.saveSauce(sauce);
    System.out.println(sauce);
    return "sauce";
  }

}
